#define _USE_MATH_DEFINES

#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>

#include "constants.h"
#include "classifier.h"
#include "EasyBMP/EasyBMP.h"
#include "liblinear-1.93/linear.h"
#include "argvparser/argvparser.h"

#include <Model/Image/Image.h>
#include <Model/Converters/Converters.h>
#include <Model/Sobel/Sobel.h>
#include <Model/SignDetector/SignDetector.h>
#include <Model/Timer/Timer.h>

using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::cout;
using std::cerr;
using std::endl;

using CommandLineProcessing::ArgvParser;

typedef vector<pair<Image<Px_8u3c> *, int> > TDataSet;
typedef vector<pair<string, int> > TFileList;
typedef vector<pair<vector<float>, int> > TFeatures;

//! Load list of files and its labels from 'data_file' and
//! stores it in 'file_list'
void LoadFileList(const string& data_file, TFileList* file_list) {
    ifstream stream(data_file.c_str());

    string filename;
    int label;
    
    int char_idx = data_file.size() - 1;
    for (; char_idx >= 0; --char_idx)
        if (data_file[char_idx] == '/' || data_file[char_idx] == '\\')
            break;
    string data_path = data_file.substr(0,char_idx+1);
    
    while(!stream.eof() && !stream.fail()) {
        stream >> filename >> label;
        if (filename.size())
            file_list->push_back(make_pair(data_path + filename, label));
    }

    stream.close();
}

//! Load images by list of files 'file_list' and store them in 'data_set'
void LoadImages(const TFileList& file_list, TDataSet* data_set) {
    for (size_t img_idx = 0; img_idx < file_list.size(); ++img_idx) {
            // Create image
        Image<Px_8u3c>* image = new Image<Px_8u3c>(file_list[img_idx].first);
            // Add image and it's label to dataset
        data_set->push_back(make_pair(image, file_list[img_idx].second));
    }
}

//! Save result of prediction to file
void SavePredictions(const TFileList& file_list,
                     const TLabels& labels, 
                     const string& prediction_file) {
        // Check that list of files and list of labels has equal size 
    assert(file_list.size() == labels.size());
        // Open 'prediction_file' for writing
    ofstream stream(prediction_file.c_str());

        // Write file names and labels to stream
    for (size_t image_idx = 0; image_idx < file_list.size(); ++image_idx)
        stream << file_list[image_idx].first << " " << labels[image_idx] << endl;
    stream.close();
}

//! Classifies angle
int sample_angle(double angle, int size) 
{
    if (angle < 0) {
        angle += 2.0 * M_PI;
    }
    angle *= size;
    angle /= 2.0 * M_PI;
    return std::min(
        size - 1, 
        static_cast<int>(std::floor(
            angle
        ))
    );
}

//! Exatract features from dataset.
//! You should implement this function by yourself =)
void ExtractFeatures(const TDataSet& data_set, TFeatures* features) {
    Timer tm;
    for (size_t image_idx = 0; image_idx < data_set.size(); ++image_idx) {
        Image<Px_8u> gs = Converters::toGrayscale(*data_set[image_idx].first);
        Image<Px_16s> sob_map;
        Image<float> sob_angle;
        auto gs16 = Converters::to16bit(gs);
        tm.resume();
        Sobel::execute_all(sob_map, sob_angle, gs16);
        tm.pause();
        int height = gs.getHeight();
        int width  = gs.getWidth();
        vector<float> one_image_features;
        for (int i = 0; i < height; i += CELL_STEP) {
            for (int j = 0; j < width; j += CELL_STEP) {
                std::vector<float> cell_features(ANGLE_BUCKETS);
                int ni = std::min(height, i + CELL_SIZE);
                int nj = std::min(width,  j + CELL_SIZE);
                for (int ii = i; ii < ni; ++ii) {
                    for (int jj = j; jj < nj; ++jj) {
                        int angle = sample_angle(sob_angle(ii, jj), ANGLE_BUCKETS);
                        float power = sob_map(ii, jj);
                        cell_features[angle] += power / 363.0f;
                    }
                }
#if 1
                double sum = 1e-6;
                for (auto it = cell_features.begin(); it != cell_features.end(); ++it) {
                    sum += *it * *it;
                }
                float div = static_cast<float>(std::sqrt(sum));
                for (auto it = cell_features.begin(); it != cell_features.end(); ++it) {
                    *it /= div;
                }
#endif
                one_image_features.insert(
                    one_image_features.end(),
                    cell_features.begin(), cell_features.end()
                );
            }
        }
#if 0
        double sum = 1e-6;
        for (auto it = one_image_features.begin(); it != one_image_features.end(); ++it) {
            sum += *it * *it;
        }
        float div = static_cast<float>(std::sqrt(sum));
        for (auto it = one_image_features.begin(); it != one_image_features.end(); ++it) {
            *it /= div;
        }
#endif
        features->push_back(make_pair(one_image_features, data_set[image_idx].second));
        // End of sample code

    }
    std::cerr << "Time: " << tm.getTotalTime() << std::endl;
}

//! Clear dataset structure
void ClearDataset(TDataSet* data_set) {
        // Delete all images from dataset
    for (size_t image_idx = 0; image_idx < data_set->size(); ++image_idx)
        delete (*data_set)[image_idx].first;
        // Clear dataset
    data_set->clear();
}

//! Train SVM classifier using data from 'data_file' and save trained model
//! to 'model_file'
void TrainClassifier(const string& data_file, const string& model_file) {
        // List of image file names and its labels
    TFileList file_list;
        // Structure of images and its labels
    TDataSet data_set;
        // Structure of features of images and its labels
    TFeatures features;
        // Model which would be trained
    TModel model;
        // Parameters of classifier
    TClassifierParams params;
    
        // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
        // Load images
    LoadImages(file_list, &data_set);
        // Extract features from images
    ExtractFeatures(data_set, &features);

        // PLACE YOUR CODE HERE
        // You can change parameters of classifier here
    params.C = 0.01;
    TClassifier classifier(params);
        // Train classifier
    classifier.Train(features, &model);
        // Save model to file
    model.Save(model_file);
        // Clear dataset structure
    ClearDataset(&data_set);
}

//! Predict data from 'data_file' using model from 'model_file' and
//! save predictions to 'prediction_file'
void PredictData(const string& data_file,
                 const string& model_file,
                 const string& prediction_file) {
        // List of image file names and its labels
    TFileList file_list;
        // Structure of images and its labels
    TDataSet data_set;
        // Structure of features of images and its labels
    TFeatures features;
        // List of image labels
    TLabels labels;

        // Load list of image file names and its labels
    LoadFileList(data_file, &file_list);
        // Load images
    LoadImages(file_list, &data_set);
        // Extract features from images
    ExtractFeatures(data_set, &features);

        // Classifier 
    TClassifier classifier = TClassifier(TClassifierParams());
        // Trained model
    TModel model;
        // Load model from file
    model.Load(model_file);
        // Predict images by its features using 'model' and store predictions
        // to 'labels'
    classifier.Predict(features, model, &labels);

        // Save predictions
    SavePredictions(file_list, labels, prediction_file);
        // Clear dataset structure
    ClearDataset(&data_set);
}

void Detect(const string& data_file)
{
    ifstream inp(data_file);
    while (true) {
        std::string str;
        std::getline(inp, str);
        if (str == "") {
            return;
        }
        std::cerr << "Processing " << str << "..." << std::endl;
        Image<Px_8u3c> img(str);
        SignDetector sd(img);
        auto res = sd.execute();
        auto it = std::find(str.rbegin(), str.rend(), '/');
        if (it == str.rend()) {
            res.save("out/" + str);
        } else {
            int p = str.size() - (it - str.rbegin()) - 1;
            res.save(str.substr(0, p) + "/out" + str.substr(p, str.size() - p));
        }
    }
}

int main(int argc, char** argv) {
#ifdef _USE_SSE
    std::cerr << "Using SSE..." << std::endl;
#endif
        // Command line options parser
    ArgvParser cmd;
        // Description of program
    cmd.setIntroductoryDescription("Machine graphics course, task 2. CMC MSU, 2013.");
        // Add help option
    cmd.setHelpOption("h", "help", "Print this help message");
        // Add other options
    cmd.defineOption("data_set", "File with dataset",
        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("model", "Path to file to save or load model",
        ArgvParser::OptionRequiresValue | ArgvParser::OptionRequired);
    cmd.defineOption("predicted_labels", "Path to file to save prediction results",
        ArgvParser::OptionRequiresValue);
    cmd.defineOption("train", "Train classifier");
    cmd.defineOption("predict", "Predict dataset");
    cmd.defineOption("detect", "Predict dataset");

        // Add options aliases
    cmd.defineOptionAlternative("data_set", "d");
    cmd.defineOptionAlternative("model", "m");
    cmd.defineOptionAlternative("predicted_labels", "l");
    cmd.defineOptionAlternative("train", "t");
    cmd.defineOptionAlternative("predict", "p");

        // Parse options
    int result = cmd.parse(argc, argv);

        // Check for errors or help option
    if (result) {
        cout << cmd.parseErrorDescription(result) << endl;
        return result;
    }

        // Get values 
    string data_file = cmd.optionValue("data_set");
    string model_file = cmd.optionValue("model");
    bool train = cmd.foundOption("train");
    bool predict = cmd.foundOption("predict");
    bool detect = cmd.foundOption("detect");

        // If we need to train classifier
    if (train)
        TrainClassifier(data_file, model_file);
        // If we need to predict data
    if (predict) {
            // You must declare file to save images
        if (!cmd.foundOption("predicted_labels")) {
            cerr << "Error! Option --predicted_labels not found!" << endl;
            return 1;
        }
            // File to save predictions
        string prediction_file = cmd.optionValue("predicted_labels");
            // Predict data
        PredictData(data_file, model_file, prediction_file);
    }
    if (detect) {
        Detect(data_file);
    }
}