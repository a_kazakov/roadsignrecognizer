#pragma once

#include <vector>

#include <Model/Image/Image.h>

#include <Model/Convolutions/Convolutions.h>

class Gaussian {
public:
    static Image<Px_16s> executeFast(const Image<Px_16s> &src, float sigma, int rad = -1);
    static Image<Px_16s> executeSlow(const Image<Px_16s> &src, float sigma, int rad = -1);
};
