#pragma once

#include <stdexcept>
#include <algorithm>

#include <Model/Image/Image.h>

class Converters {
public:
    static Image<Px_8u> toGrayscale(const Image<Px_8u3c> &src);

    template<typename PxType1, typename PxType2>
    static Image<PxType2> convertImgType(const Image<PxType1> &src)
    {
        Image<PxType2> dst = createSimilarImage<PxType2>(src);
        int total_pixels = src.getNumPixels();
        const PxType1 *psrc = src.getFirstPtr();
        PxType2 *pdst = dst.getFirstPtr();
        for (int i = 0; i < total_pixels; ++i) {
            *pdst = static_cast<PxType2>(*psrc);
            ++psrc;
            ++pdst;
        }
        return dst;
    }

    static Image<Px_32s> to32bit(const Image<Px_8u> &src);
    static Image<Px_16s> to16bit(const Image<Px_8u> &src);
    static Image<Px_8u>  to8bit(const Image<Px_16s> &src, int min = 0, int max = 255);
    static Image<Px_16s3c> toYUV(const Image<Px_8u3c> &src);
};