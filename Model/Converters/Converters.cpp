#include "Converters.h"

Image<Px_8u> Converters::toGrayscale(const Image<Px_8u3c> &src)
{
	Image<Px_8u> dst = createSimilarImage<Px_8u>(src);
	int total_pixels    = src.getNumPixels();
	const Px_8u3c *psrc = src.getFirstPtr();
	Px_8u *pdst         = dst.getFirstPtr();
	for (int i = 0; i < total_pixels; ++i) {
		*pdst = static_cast<uint8_t>(
			(
				 721 * static_cast<uint32_t>((*psrc)[0]) +
				7154 * static_cast<uint32_t>((*psrc)[1]) +
				2125 * static_cast<uint32_t>((*psrc)[2])
			) / 10000
		);
		++psrc;
		++pdst;
	}
	return dst;
}

Image<Px_32s> Converters::to32bit(const Image<Px_8u> &src)
{
	return convertImgType<Px_8u, Px_32s>(src);
}

Image<Px_16s> Converters::to16bit(const Image<Px_8u> &src)
{
	return convertImgType<Px_8u, Px_16s>(src);
}

Image<Px_8u> Converters::to8bit(const Image<Px_16s> &src, int min, int max)
{
	Image<Px_8u> dst = createSimilarImage<Px_8u, Px_16s>(src);
	int total_pixels   = src.getNumPixels();
	const Px_16s *psrc = src.getFirstPtr();
	Px_8u *pdst        = dst.getFirstPtr();
    if (max == min) {
        ++max;
    }
	for (int i = 0; i < total_pixels; ++i) {
		*pdst = static_cast<Px_8u>(std::min(std::max(
			static_cast<float>(*psrc - min) / (max - min) * 255
			, 0.0f), 255.0f));
		++psrc;
		++pdst;
	}
	return dst;
}

Image<Px_16s3c> Converters::toYUV(const Image<Px_8u3c> &bgr)
{
    Image<Px_16s3c> yuv = createSimilarImage<Px_16s3c>(bgr);
    for (int i = 0; i < yuv.getHeight(); ++i) {
        for (int j = 0; j < yuv.getWidth(); ++j) {
            yuv(i, j)[0] = (299 * int(bgr(i, j)[2]) + 587 * int(bgr(i, j)[1]) + 114 * int(bgr(i, j)[0])) / 1000;
            yuv(i, j)[1] = (-147 * int(bgr(i, j)[2]) - 289 * int(bgr(i, j)[1]) + 436 * int(bgr(i, j)[0])) / 1000;
            yuv(i, j)[2] = (615 * int(bgr(i, j)[2]) - 515 * int(bgr(i, j)[1]) - 100 * int(bgr(i, j)[0])) / 1000;
        }
    }
    return yuv;
}
