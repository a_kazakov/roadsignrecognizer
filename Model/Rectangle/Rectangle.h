#pragma once

#include <algorithm>

struct Rectangle
{
    Rectangle(int y = std::numeric_limits<int>::max(), 
              int x = std::numeric_limits<int>::max(), 
              int h = 1, int w = 1);
    Rectangle &operator *= (const Rectangle &r);
    Rectangle &operator += (const Rectangle &r);
    bool intersect(Rectangle r);
	int y, x, h, w;
};

