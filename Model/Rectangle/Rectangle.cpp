#include "Rectangle.h"

Rectangle::Rectangle(int y, int x, int h, int w)
{
	this->y = y;
	this->x = x;
	this->h = h;
	this->w = w;
}

Rectangle &Rectangle::operator *= (const Rectangle &r)
{
    int y1 = std::max(y, r.y);
    int x1 = std::max(x, r.x);
    int y2 = std::min(y + h, r.y + r.h);
    int x2 = std::min(x + w, r.x + r.w);
    y = y1;
    x = x1;
    h = y2 - y;
    w = x2 - x;
    return *this;
}

Rectangle &Rectangle::operator += (const Rectangle &r)
{
    int y1 = std::min(y, r.y);
    int x1 = std::min(x, r.x);
    int y2 = std::max(y + h, r.y + r.h);
    int x2 = std::max(x + w, r.x + r.w);
    y = y1;
    x = x1;
    h = y2 - y;
    w = x2 - x;
    return *this;
}

bool Rectangle::intersect(Rectangle r) 
{
    r *= (*this);
    return r.h > 0 && r.w > 0;
}
