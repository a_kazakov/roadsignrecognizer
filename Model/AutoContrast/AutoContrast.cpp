#include "AutoContrast.h"

void AutoContrast::build_hist(std::vector<int> &out, const Image<Px_8u> &img)
{
	out.assign(256, 0);
	for (int i = 0; i < img.getHeight(); ++i) {
		for (int j = 0; j < img.getWidth(); ++j) {
			++out[img(i, j)];
		}
	}
}

std::pair<int, int> AutoContrast::find_bounds(const std::vector<int> &hist, const Image<Px_8u> &img, float fraction)
{
	int limit = static_cast<int>(img.getHeight() * img.getWidth() * fraction);
	std::pair<int, int> res;
	int s = 0;
	for (int i = 0; i < static_cast<int>(hist.size()); ++i) {
		s += hist[i];
		if (s > limit) {
			res.first = i;
			break;
		}
	}
	s = 0;
    for (int i = static_cast<int>(hist.size()) - 1; i >= 0; --i) {
		s += hist[i];
		if (s > limit) {
			res.second = i;
			break;
		}
	}
	return res;
}

void AutoContrast::execute(Image<Px_8u3c> &img, float fraction)
{
	std::vector<int> hist;
	Image<Px_8u> gs = Converters::toGrayscale(img);
	build_hist(hist, gs);
	std::pair<int, int> bounds = find_bounds(hist, gs, fraction);
	Image<Px_8u> c1, c2, c3;
	ImgOperations::splitChannels(c1, c2, c3, img);
	// Mega-cheat
    c1 = Converters::to8bit(Converters::to16bit(c1), bounds.first, bounds.second);
    c2 = Converters::to8bit(Converters::to16bit(c2), bounds.first, bounds.second);
    c3 = Converters::to8bit(Converters::to16bit(c3), bounds.first, bounds.second);

	img = ImgOperations::mixChannels(c1, c2, c3);
}
