#pragma once

#include <Model/Image/Image.h>
#include <Model/Converters/Converters.h>
#include <Model/ImgOperations/ImgOperations.h>

#include <algorithm>
#include <vector>

class AutoContrast {
public:
    static void execute(Image<Px_8u3c> &img, float fraction = 0);
private:
    static void build_hist(std::vector<int> &out, const Image<Px_8u> &img);
    static std::pair<int, int> find_bounds(const std::vector<int> &hist, const Image<Px_8u> &img, float fraction);
};
