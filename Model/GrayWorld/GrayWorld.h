#pragma once

#include <Model/Image/Image.h>

#include <algorithm>

class GrayWorld {
public:
    static void execute(Image<Px_8u3c> &img);
};
