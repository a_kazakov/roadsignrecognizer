#include "GrayWorld.h"

void GrayWorld::execute(Image<Px_8u3c> &img)
{
	int sum[3] ={ 0, 0, 0 };
	for (int i = 0; i < img.getHeight(); ++i) {
		for (int j = 0; j < img.getWidth(); ++j) {
			for (int k = 0; k < 3; ++k) {
				sum[k] += img(i, j)[k];
			}
		}
	}
	int s = sum[0] + sum[1] + sum[2];
    bool zero[3];
	float m[3];
	for (int k = 0; k < 3; ++k) {
        zero[k] = (sum[k] == 0);
        m[k] = static_cast<float>(s) / 3.0f / static_cast<float>(sum[k]);
	}
	for (int i = 0; i < img.getHeight(); ++i) {
		for (int j = 0; j < img.getWidth(); ++j) {
			for (int k = 0; k < 3; ++k) {
				img(i, j)[k] = std::min(std::max(
                    zero[k]
                    ? static_cast<int>(std::round(s / (3.0f * img.getHeight() * img.getWidth())))
                    : static_cast<int>(std::round(m[k] * img(i, j)[k]))
                , 0), 255);
			}
		}
	}
}