#pragma once

#include <Model/Image/Image.h>

class DSU {
public:
    DSU(int height, int width);
    void merge(std::pair<int, int> a, std::pair<int, int> b);
    int getSize(std::pair<int, int> a);
    Image<int> getObjects();
protected:
    struct Item {
        int y, x;
        int size;
        int label = 0;
    };
    Image<Item> data;
    void toTop(std::pair<int, int> &x);
};