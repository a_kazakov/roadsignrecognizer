#include "DSU.h"

DSU::DSU(int height, int width) :
data(height, width)
{
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            data(i, j).y = i;
            data(i, j).x = j;
            data(i, j).size = 1;
        }
    }
}

void DSU::merge(std::pair<int, int> a, std::pair<int, int> b) 
{
    toTop(a);
    toTop(b);
    if (a != b) {
        auto *it_a = &data(a.first, a.second);
        auto *it_b = &data(b.first, b.second);
        if (it_a->size < it_b->size) {
            std::swap(it_a, it_b);
            std::swap(a, b);
        }
        it_b->y = a.first;
        it_b->x = a.second;
        it_a->size += it_b->size;
    }
}

int DSU::getSize(std::pair<int, int> a) 
{
    toTop(a);
    return data(a.first, a.second).size;
}

Image<int> DSU::getObjects() 
{
    for (int i = 0; i < data.getHeight(); ++i) {
        for (int j = 0; j < data.getWidth(); ++j) {
            data(i, j).label = 0;
        }
    }
    Image<int> res = createSimilarImage<int>(data);
    int cnt = 1;
    for (int i = 0; i < data.getHeight(); ++i) {
        for (int j = 0; j < data.getWidth(); ++j) {
            std::pair<int, int> p = std::make_pair(i, j);
            toTop(p);
            auto &el = data(p.first, p.second);
            if (el.size < 10) {
                res(i, j) = 0;
            } else {
                if (el.label == 0) {
                    el.label = cnt++;
                }
                res(i, j) = el.label;
            }
        }
    }
    return res;
}

void DSU::toTop(std::pair<int, int> &a)
{
    std::pair<int, int> new_a;
    new_a.first = data(a.first, a.second).y;
    new_a.second = data(a.first, a.second).x;
    if (new_a == a) {
        return;
    }
    toTop(new_a);
    data(a.first, a.second).y = new_a.first;
    data(a.first, a.second).x = new_a.second;
    a = new_a;
}
