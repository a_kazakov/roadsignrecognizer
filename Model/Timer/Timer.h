#pragma once

/**
    @file Timer.h 
    @brief Contains class for measuring code execution time
    @author Artem Kazakov
*/

#include <ctime>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <cstdint>

/**
    @class Timer
    @brief Measures code execution time
    Usage:
    @code
    Timer t;
    for (int i = 0; i < n; ++i) {
        // some code
        t.resume();
        // measured code
        t.pause();
        // some code
    }
    std::cout << "Result: " << t.getTotalTime << " seconds." << std::endl;
    @endcode
*/
class Timer
{
public:
    //! @brief Creates paused timer
    Timer();
    //! @brief Reinializes timer (and pauses it)
    void restart();
    //! @brief Resumes execution after pause or initialization
    void resume();
    //! @brief Pauses timer
    void pause();
    //! @brief Returns total measured time
    //! @return Total time in seconds
    double getTotalTime();
private:
    int64_t _time_passed;
    int64_t _latest_mark;
    //! @brief Get current time value in system units
    //! @return Time values in internal system units
    inline int64_t getTimeValue();
    //! @brief Converts time system in system units to seconds
    //! @param t_val time value in system units
    //! @return the same time, but in seconds
    inline double toSeconds(int64_t t_val);
};