#include "Timer.h"

Timer::Timer()
{
    restart();
}

void Timer::restart() 
{
    _time_passed = 0;
    _latest_mark = getTimeValue();
}

void Timer::resume()
{
    _latest_mark = getTimeValue();
}

void Timer::pause()
{
    _time_passed += getTimeValue() - _latest_mark;
}

double Timer::getTotalTime()
{
    return toSeconds(_time_passed);
}

double Timer::toSeconds(int64_t x)
{
    return static_cast<double>(x) / static_cast<double>(CLOCKS_PER_SEC);
}

int64_t Timer::getTimeValue()
{
    return clock();
}
