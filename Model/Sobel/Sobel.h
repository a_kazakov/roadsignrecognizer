#pragma once

#include <sse_utils.h>

#include <algorithm>
#include <cmath>

#include <Model/Image/Image.h>
#include <Model/Convolutions/Convolutions.h>

class Sobel {
public:
    static Image<Px_16s> execute_y(const Image<Px_16s> &src);
    static Image<Px_16s> execute_x(const Image<Px_16s> &src);
    static Image<Px_16s> execute_map(const Image<Px_16s> &src);
    static void execute_all(Image<Px_16s> &out_map, Image<float> &out_angle, const Image<Px_16s> &src);
};
