#include "Sobel.h"

Image<Px_16s> Sobel::execute_y(const Image<Px_16s> &src)
{
    src.checkBorders(5);
    src.fillBorders();
    Image<int> k(3, 3);
    int rk[9] = { -1, -2, -1, 0, 0, 0, 1, 2, 1 };
    std::memcpy(k.ptr(0, 0), rk, 9 * sizeof(int));
    if (support_sse2()) {
        return Convolutions::execute_sse(src, k);
    }
    return Convolutions::execute(src, k);
}

Image<Px_16s> Sobel::execute_x(const Image<Px_16s> &src)
{
	src.checkBorders(5);
	src.fillBorders();
	Image<int> k(3, 3);
	int rk[9] = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };
	std::memcpy(k.ptr(0, 0), rk, 9 * sizeof(int));
    if (support_sse2()) {
        return Convolutions::execute_sse(src, k);
    }
    return Convolutions::execute(src, k);
}

Image<Px_16s> Sobel::execute_map(const Image<Px_16s> &src)
{
	Image<Px_16s> res_g = createSimilarImage<Px_16s>(src);
	Image<Px_16s> tmp_y = execute_y(src);
	Image<Px_16s> tmp_x = execute_x(src);
	for (int i = 0; i < src.getHeight(); ++i) {
		Px_16s *py = tmp_y.ptr(i, 0);
		Px_16s *px = tmp_x.ptr(i, 0);
		Px_16s *pg = res_g.ptr(i, 0);
		for (int j = 0; j < src.getWidth(); ++j) {
			*pg = static_cast<Px_16s>(std::sqrt(static_cast<float>(*py * *py + *px * *px)));
			++py, ++px, ++pg;
		}
	}
	return res_g;
}

void Sobel::execute_all(Image<Px_16s> &out_map, Image<float> &out_angle, const Image<Px_16s> &src)
{
    out_map = createSimilarImage<Px_16s>(src);
    out_angle = createSimilarImage<float>(src);
    Image<Px_16s> tmp_y = execute_y(src);
    Image<Px_16s> tmp_x = execute_x(src);
    for (int i = 0; i < src.getHeight(); ++i) {
        Px_16s *py = tmp_y.ptr(i, 0);
        Px_16s *px = tmp_x.ptr(i, 0);
        Px_16s *pm = out_map.ptr(i, 0);
        float  *pa = out_angle.ptr(i, 0);
        for (int j = 0; j < src.getWidth(); ++j) {
            *pm = static_cast<Px_16s>(std::round(std::sqrt(static_cast<float>(*py * *py + *px * *px))));
            *pa = static_cast<float>(std::atan2(*py, *px));
            ++py, ++px, ++pm, ++pa;
        }
    }
}
