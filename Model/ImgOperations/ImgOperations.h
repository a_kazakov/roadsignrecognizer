#pragma once

#include <algorithm>
#include <vector>

#include <Model/Image/Image.h>

class ImgOperations {
public:
    static void setShiftedROI(Image<Px_8u> &img, int my, int mx);
    static Image<Px_8u3c> mixChannels(const Image<Px_8u> &src1, const Image<Px_8u> &src2, const Image<Px_8u> &src3);
    static void splitChannels(Image<Px_8u> &out1, Image<Px_8u> &out2, Image<Px_8u> &out3, const Image<Px_8u3c> &src);

    template <typename PxType>
    static Image<PxType> resizeImage(const Image<PxType> &src, int new_h, int new_w)
    {
        // bilinear resize
        Image<PxType> res(new_h, new_w);

        std::vector<int> px_h_v;
        std::vector<float> fr_h_v;

        for (int j = 0; j < res.getWidth(); ++j) {
            float h = static_cast<float>(j) / (res.getWidth() - 1) * (src.getWidth() - 1);
            int px_h = std::min(std::max(static_cast<int>(floor(h)), 0), src.getWidth() - 2);
            float fr_h = h - px_h;
            px_h_v.push_back(px_h);
            fr_h_v.push_back(fr_h);
        }

        for (int i = 0; i < res.getHeight(); ++i) {
            PxType *pres = res.ptr(i, 0);

            float v = static_cast<float>(i) / (res.getHeight() - 1) * (src.getHeight() - 1);
            int px_v = std::min(std::max(static_cast<int>(floor(v)), 0), src.getHeight() - 2);
            float fr_v = v - px_v;

            for (int j = 0; j < res.getWidth(); ++j) {
                int px_h = px_h_v[j];
                float fr_h = fr_h_v[j];

                // Neighbours
                PxType p1 = src(px_v, px_h);
                PxType p2 = src(px_v + 1, px_h);
                PxType p3 = src(px_v + 1, px_h + 1);
                PxType p4 = src(px_v, px_h + 1);

                // Ratios
                float r1 = (1 - fr_v) * (1 - fr_h);
                float r2 = fr_v * (1 - fr_h);
                float r3 = fr_v * fr_h;
                float r4 = (1 - fr_v) * fr_h;

                *pres = static_cast<PxType>(p1 * r1 + p2 * r2 + p3 * r3 + p4 * r4 + 0.5);
                ++pres;
            }
        }
        return res;
    }
};
