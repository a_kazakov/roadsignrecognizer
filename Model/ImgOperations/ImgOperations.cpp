#include "ImgOperations.h"

void ImgOperations::setShiftedROI(Image<Px_8u> &src, int my, int mx)
{
	src.resetRoi();
	src.setRoi(my, mx, src.getHeight(), src.getWidth());
}

Image<Px_8u3c> ImgOperations::mixChannels(const Image<Px_8u> &src1, const Image<Px_8u> &src2, const Image<Px_8u> &src3)
{
	// Width and height are taken from src1
	Image<Px_8u3c> res(src1.getHeight(), src1.getWidth(), src1.getBorderWidth());
	for (int i = 0; i < src1.getHeight(); ++i) {
		for (int j = 0; j < src1.getWidth(); ++j) {
			res(i, j)[0] = src1(i, j);
			res(i, j)[1] = src2(i, j);
			res(i, j)[2] = src3(i, j);
		}
	}
	return res;
}

void ImgOperations::splitChannels(Image<Px_8u> &out1, Image<Px_8u> &out2, Image<Px_8u> &out3, const Image<Px_8u3c> &src)
{
	Image<Px_8u> res1, res2, res3;
	res1 = createSimilarImage<Px_8u>(src);
	res2 = createSimilarImage<Px_8u>(src);
	res3 = createSimilarImage<Px_8u>(src);
	for (int i = 0; i < src.getHeight(); ++i) {
		for (int j = 0; j < src.getWidth(); ++j) {
			res1(i, j) = src(i, j)[0];
			res2(i, j) = src(i, j)[1];
			res3(i, j) = src(i, j)[2];
		}
	}
	out1 = res1;
	out2 = res2;
	out3 = res3;
}