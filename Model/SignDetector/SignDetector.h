#pragma once

#include <Model/Image/Image.h>
#include <Model/Converters/Converters.h>
#include <Model/GrayWorld/GrayWorld.h>
#include <Model/AutoContrast/AutoContrast.h>
#include <Model/DSU/DSU.h>
#include <Model/Rectangle/Rectangle.h>
#include <Model/Sobel/Sobel.h>

#include <vector>
#include <sstream>
#include <queue>

class SignDetector
{
public:
    SignDetector(const Image<Px_8u3c> &src);
    ~SignDetector();
    Image<Px_8u3c> execute();
protected:
    Image<Px_8u3c> img;
    Image<Px_8u> bin;
    std::vector<Rectangle> rects;
    void delRectangle(int idx);
    int getRectSum(int idx);
    bool splitRect(int idx);
private:
    static bool isAcceptable(int y, int u, int v);
    void drawVLine(int y1, int y2, int x);
    void drawHLine(int y, int x1, int x2);
    void drawRect(const Rectangle &rect);
};

