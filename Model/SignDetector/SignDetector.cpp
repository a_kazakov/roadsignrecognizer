#include "SignDetector.h"

SignDetector::SignDetector(const Image<Px_8u3c> &src)
{
    img.assign(src, true);
    bin = createSimilarImage<Px_8u>(src);
}

SignDetector::~SignDetector()
{
}

Image<Px_8u3c> SignDetector::execute()
{
//    GrayWorld::execute(img);
    DSU dsu(img.getHeight(), img.getWidth());
    for (int i = 0; i < img.getHeight(); ++i) {
        for (int j = 0; j < img.getWidth(); ++j) {
            auto &px = img(i, j);
            if (isAcceptable(px[0], px[1], px[2])) {
                if (i > 0) {
                    auto &px_top = img(i - 1, j);
                    if (isAcceptable(px_top[0], px_top[1], px_top[2])) {
                        dsu.merge(std::make_pair(i, j), std::make_pair(i - 1, j));
                    }
                    if (j > 0) {
                        auto &px_lefttop = img(i - 1, j - 1);
                        if (isAcceptable(px_lefttop[0], px_lefttop[1], px_lefttop[2])) {
                            dsu.merge(std::make_pair(i, j), std::make_pair(i - 1, j - 1));
                        }
                    }
                    if (j < img.getWidth() - 1) {
                        auto &px_righttop = img(i - 1, j + 1);
                        if (isAcceptable(px_righttop[0], px_righttop[1], px_righttop[2])) {
                            dsu.merge(std::make_pair(i, j), std::make_pair(i - 1, j + 1));
                        }
                    }
                }
                if (j > 0) {
                    auto &px_left = img(i, j - 1);
                    if (isAcceptable(px_left[0], px_left[1], px_left[2])) {
                        dsu.merge(std::make_pair(i, j), std::make_pair(i, j - 1));
                    }
                }
            }
        }
    }
    Image<int> spl = dsu.getObjects();
    for (int i = 0; i < spl.getHeight(); ++i) {
        for (int j = 0; j < spl.getWidth(); ++j) {
            bin(i, j) = spl(i, j) == 0 ? 0 : 255;
            if (spl(i, j) == 0) {
                //img(i, j)[0] = 0;
                //img(i, j)[1] = 0;
                //img(i, j)[2] = 0;
                continue;
            }
            if (static_cast<int>(rects.size()) < spl(i, j)) {
                rects.resize(spl(i, j));
            }
            if (rects[spl(i, j)].y == std::numeric_limits<int>::max()) {
                rects[spl(i, j) - 1] = Rectangle(i, j, 1, 1);
            } else {
                rects[spl(i, j) - 1] += Rectangle(i, j, 1, 1);
            }
        }
    }

    // kill by tresholds
    for (size_t i = 0; i < rects.size(); ++i) {
        int s = getRectSum(i);
        if (!(100 < s && s < 5000)) {
            delRectangle(i);
            --i;
        }
    }


    for (size_t i = 0; i < rects.size(); ++i) {
        for (size_t j = 0; j < rects.size(); ++j) {
            if (i == j) {
                continue;
            }
            if (std::abs(rects[i].x - rects[j].x) + std::abs(rects[i].w - rects[j].w) <= 5) {
                rects[i] += rects[j];
                delRectangle(j);
                --j, --i;
                break;
            }
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        if (float(std::min(rects[i].h, rects[i].w)) / std::max(rects[i].h, rects[i].w) < 0.3f) {
            delRectangle(i);
            --i;
            continue;
        }
        if (rects[i].h * rects[i].w > 10000) {
            delRectangle(i);
            --i;
            continue;
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        for (size_t j = 0; j < rects.size(); ++j) {
            if (i == j) {
                continue;
            }
            if (rects[i].intersect(rects[j])) {
                if (i < j) {
                    rects[i] += rects[j];
                    delRectangle(j);
                    --j;
                    continue;
                } else {
                    rects[j] += rects[i];
                    delRectangle(i);
                    --i;
                    break;
                }
            }
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        if (float(rects[i].w) / rects[i].h > 1.3f) {
            delRectangle(i);
            --i;
            continue;
        }
        if (float(rects[i].h) / rects[i].w > 2.7f) {
            delRectangle(i);
            --i;
            continue;
        }
        if (float(rects[i].h) / rects[i].w > 1.7f) {
            if (!splitRect(i)) {
                --i;
                continue;
            }
        }
        if (rects[i].h * rects[i].w > 20000) {
            delRectangle(i);
            --i;
            continue;
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        bin.setRoi(rects[i].y, rects[i].x, rects[i].h, rects[i].w);
        Image<Px_8u> t = bin.createFromROI();
        t = ImgOperations::resizeImage(t, 50, 50);
        auto sb = Sobel::execute_map(Converters::to16bit(t));
        int s = 0;
        for (int ii = 0; ii < sb.getHeight(); ++ii) {
            for (int jj = 0; jj < sb.getWidth(); ++jj) {
                s += !!sb(ii, jj);
            }
        }
        bin.resetRoi();
        if (!(530 <= s && s <= 1570)) {
            delRectangle(i);
            --i;
        }
    }
    static int k = 10000;
    for (size_t i = 0; i < rects.size(); ++i) {
        bin.setRoi(rects[i].y, rects[i].x, rects[i].h, rects[i].w);
        Image<Px_8u> t = bin.createFromROI();
        t = ImgOperations::resizeImage(t, 50, 50);
        auto sb = Sobel::execute_map(Converters::to16bit(t));
        int s1 = 0;
        int s2 = 0;
        for (int ii = 0; ii < t.getHeight(); ++ii) {
            for (int jj = 0; jj < t.getWidth() / 2; ++jj) {
                s1 += t(ii, jj) != t(ii, t.getWidth() - jj - 1);
                s2 += !!sb(ii, jj) + !!sb(ii, t.getWidth() - jj - 1);
            }
        }
        if (s1 > 700 && (s1 > 900 || s2 > 1100)) {
            delRectangle(i);
            --i;
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        bin.setRoi(rects[i].y, rects[i].x, rects[i].h, rects[i].w);
        Image<Px_8u> t = bin.createFromROI();
        t = ImgOperations::resizeImage(t, 50, 50);
        int s = 0;
        for (int ii = 0; ii < t.getHeight(); ++ii) {
            for (int jj = 0; jj < t.getWidth(); ++jj) {
                s += !!t(ii, jj);
            }
        }
        if (rects[i].w < 20 && (s > 1750)) {
            delRectangle(i);
            --i;
        }
    }
    for (size_t i = 0; i < rects.size(); ++i) {
        drawRect(rects[i]);
/*        if (rects[i].w < 20 && (s > 1750)) {
            std::stringstream ss;
            ss << "tmp/" << s << "_" << k++ << ".bmp";
            t.save(ss.str());
        }*/
    }
    return img;
}

bool SignDetector::splitRect(int idx)
{
    int p = 0;
    int pv = std::numeric_limits<int>::max();
    for (int j = rects[idx].y + rects[idx].h / 3; j < rects[idx].y + rects[idx].h * 2 / 3; ++j) {
        int s = 0;
        for (int k = rects[idx].x; k < rects[idx].x + rects[idx].w; ++k) {
            if (bin(j, k) != 0) {
                ++s;
            }
        }
        if (s < pv) {
            pv = s;
            p = j;
        }
    }
    if (pv < rects[idx].w / 10) {
        rects.push_back(Rectangle(p, rects[idx].x, rects[idx].y + rects[idx].h - p, rects[idx].w));
        rects[idx].h = p - rects[idx].y;
        return true;
    } else {
        delRectangle(idx);
        return false;
    }
}

void SignDetector::delRectangle(int idx) 
{
    std::swap(rects[idx], rects[rects.size() - 1]);
    rects.pop_back();
}

int SignDetector::getRectSum(int idx) 
{
    int res = 0;
    for (int i = rects[idx].y; i < rects[idx].y + rects[idx].h; ++i) {
        for (int j = rects[idx].x; j < rects[idx].x + rects[idx].w; ++j) {
            res += !!bin(i, j);
        }
    }
    return res;
}

bool SignDetector::isAcceptable(int y, int u, int v) 
{
    return (1.5 * u < v && v < 1000.0 * u &&
            1.0 * y < v && v < 1000.0 * y &&
            0.0 * y < u && u < 1000.0 * y) ||
           (0.0 * u < v && v < 1000.0 * u &&
            0 * y < v && v < 1 * y &&
            0 * y < u && u < 0.5 * y);
    if (!(2 < y && y < 230)) {
        return false;
    }
    if (u - v < 0 && u + v > 10) {
        return true;
    }
    /*if (y < 128 && u - v > 30 && 2 * u + v > 30) {
        return true;
    }*/
    return false;
}

void SignDetector::drawVLine(int y1, int y2, int x) {
    for (int y = y1; y <= y2; ++y) {
        img(y, x)[0] = 0;
        img(y, x)[1] = 255;
        img(y, x)[2] = 0;
    }
}

void SignDetector::drawHLine(int y, int x1, int x2) {
    for (int x = x1; x <= x2; ++x) {
        img(y, x)[0] = 0;
        img(y, x)[1] = 255;
        img(y, x)[2] = 0;
    }
}

void SignDetector::drawRect(const Rectangle &rect)
{
    drawVLine(rect.y, rect.y + rect.h - 1, rect.x);
    drawVLine(rect.y, rect.y + rect.h - 1, rect.x + rect.w - 1);
    drawHLine(rect.y, rect.x, rect.x + rect.w - 1);
    drawHLine(rect.y + rect.h - 1, rect.x, rect.x + rect.w - 1);
}