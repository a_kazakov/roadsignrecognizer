#pragma once

#include <vector>
#include <thread>
#include <algorithm>

#include <sse_utils.h>

#define NO_MT

#include <Model/Image/Image.h>

class Convolutions {
public:
    static Image<Px_16s> execute(const Image<Px_16s> &src, const Image<int> &conv, int divisor = 1);
    static Image<Px_16s> execute_sse(const Image<Px_16s> &src, const Image<int> &conv, int divisor = 1);
    static Image<Px_16s> execute_vert(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor = 1);
    static Image<Px_16s> execute_hor(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor = 1);
protected:
    static Image<Px_16s> execute_mt(const Image<Px_16s> &src, const Image<int> &conv, int divisor);
    static Image<Px_16s> applyLinearConvolution_mt(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2);
    static Image<Px_16s> applyLinearConvolution(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2);
};
