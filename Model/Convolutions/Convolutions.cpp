#include "Convolutions.h"

Image<Px_16s> Convolutions::execute_mt(const Image<Px_16s> &src, const Image<int> &conv, int divisor)
{
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rv = conv.getHeight() / 2;
	int rh = conv.getWidth() / 2;
	std::vector<std::thread> active_threads;
	int threads = std::thread::hardware_concurrency();
	int start_offset = rv * src.getStride() + rh;
	int next_row_offset = src.getStride() - conv.getWidth();
	auto start_pconv = conv.ptr(0, 0);
	for (int t = 0; t < threads; ++t) {
		active_threads.push_back(std::thread([=, &src, &dst, &conv](int f, int t, int step) {
			for (int i = f; i < t; i += step) {
				auto *pdst = dst.ptr(i, 0);
				auto *psrc = src.ptr(i, 0);
				for (int j = 0; j < src.getWidth(); ++j) {
					int s = 0;
					auto *lpsrc = psrc - start_offset;
					auto *pconv = start_pconv;
					for (int ii = 0; ii < conv.getHeight(); ++ii) {
						for (int jj = 0; jj < conv.getWidth(); ++jj) {
							s += static_cast<int>(*lpsrc) * *pconv;
							++lpsrc, ++pconv;
						}
						lpsrc += next_row_offset;
					}
					*pdst = s / divisor;
					++pdst;
					++psrc;
				}
			}
		}, t, src.getHeight(), threads));
	}
	std::for_each(active_threads.begin(), active_threads.end(), [](std::thread &t) {
		t.join();
	});
	return dst;
}

#define ALIGN 16

void *aligned_malloc(int size) {
    char *mem = reinterpret_cast<char *>(malloc(size + ALIGN + sizeof(void*)));
    void **ptr = (void**) ((long) (mem + ALIGN + sizeof(void*)) & ~(ALIGN - 1));
    ptr[-1] = mem;
    return ptr;
}

void aligned_free(void *ptr) {
    free(((void**) ptr)[-1]);
}

// requires additional 4px borders
Image<Px_16s> Convolutions::execute_sse(const Image<Px_16s> &src, const Image<int> &conv, int divisor)
{
    Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
    std::vector<int16_t *> aconv(conv.getHeight());
    for (int i = 0; i < conv.getHeight(); ++i) {
        aconv[i] = reinterpret_cast<int16_t *>(aligned_malloc((conv.getWidth() + 16) * sizeof(Px_16s)));
        std::memset(aconv[i], 0, (conv.getHeight() + 16) * sizeof(int16_t));
        for (int j = 0; j < conv.getWidth(); ++j) {
            aconv[i][j] = conv(i, j);
        }
    }
    int rv = conv.getHeight() / 2; 
    int rh = conv.getWidth() / 2;
    int start_offset = rv * src.getStride() + rh;
    int next_row_offset = src.getStride();
    auto start_pconv = conv.ptr(0, 0);
    for (int i = 0; i < src.getHeight(); ++i) {
        auto *pdst = dst.ptr(i, 0);
        auto *psrc = src.ptr(i, 0);
        for (int j = 0; j < src.getWidth(); ++j) {
#ifdef _MSC_VER
            __declspec(align(16)) int32_t buf[4] = { 0 };
            __declspec(align(16)) int16_t l1[8] = { 0 }, l2[8] = { 0 };
#else
            int32_t buf[4] __attribute__((aligned(16))) = { 0 };
            int16_t  l1[8] __attribute__((aligned(16))) = { 0 };
            int16_t  l2[8] __attribute__((aligned(16))) = { 0 };
#endif
            auto *lpsrc = psrc - start_offset;
            __m128i *buf128 = reinterpret_cast<__m128i *>(buf);
            __m128i t1, t2, tr, ts = *reinterpret_cast<__m128i *>(buf);
            for (int ii = 0; ii < conv.getHeight(); ++ii) {
                const __m128i *conv_row128 = reinterpret_cast<const __m128i *>(aconv[ii]);
                const __m128i *img_row128  = reinterpret_cast<const __m128i *>(lpsrc);
                for (int jj = 0; jj < conv.getWidth(); jj += sizeof(__m128i) / sizeof(Px_16s)) {
                    t1 = _mm_loadu_si128(img_row128);
                    t2 = _mm_load_si128(conv_row128);
                    tr = _mm_madd_epi16(t1, t2);
                    ts = _mm_add_epi32(ts, tr);
                    ++conv_row128, ++img_row128;
                }
                lpsrc += next_row_offset;
            }
            *buf128 = ts;
            int res = buf[0] + buf[1] + buf[2] + buf[3];
            *pdst = res / divisor;
            ++pdst;
            ++psrc;
        }
    }
    for (int i = 0; i < conv.getHeight(); ++i) {
        aligned_free(aconv[i]);
    }
    return dst;
}

Image<Px_16s> Convolutions::execute(const Image<Px_16s> &src, const Image<int> &conv, int divisor)
{
#ifndef NO_MT
    if (std::thread::hardware_concurrency() > 1) {
        try {
            return execute_mt(src, conv, divisor);
        }
        catch (std::exception &ex) {
            std::cerr << "MT error: " << ex.what() << std::endl;
        }
    }
#endif
    Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
    int rv = conv.getHeight() / 2;
    int rh = conv.getWidth() / 2;
    int start_offset = rv * src.getStride() + rh;
    int next_row_offset = src.getStride() - conv.getWidth();
    auto start_pconv = conv.ptr(0, 0);
    for (int i = 0; i < src.getHeight(); ++i) {
        auto *pdst = dst.ptr(i, 0);
        auto *psrc = src.ptr(i, 0);
        for (int j = 0; j < src.getWidth(); ++j) {
            int s = 0;
            auto *lpsrc = psrc - start_offset;
            auto *pconv = start_pconv;
            for (int ii = 0; ii < conv.getHeight(); ++ii) {
                for (int jj = 0; jj < conv.getWidth(); ++jj) {
                    s += static_cast<int>(*lpsrc) * *pconv;
                    ++lpsrc, ++pconv;
                }
                lpsrc += next_row_offset;
            }
            *pdst = s / divisor;
            ++pdst;
            ++psrc;
        }
    }
    return dst;
}

Image<Px_16s> Convolutions::applyLinearConvolution_mt(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2)
{
	if (conv.size() % 2 == 0) {
		throw std::runtime_error("Size of Convolution kernel must be odd.");
	}
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rad = conv.size() / 2;
	int d = 2 * rad + 1;
	std::vector<std::thread> active_threads;
	int threads = std::thread::hardware_concurrency();
	for (int t = 0; t < threads; ++t) {
		active_threads.push_back(std::thread([=, &src, &dst, &conv](int f, int t, int step) {
			for (int i = f; i < t; i += step) {
				const Px_16s *psrc = src.ptr(i, x1);
				Px_16s *pdst = dst.ptr(i, x1);
				for (int j = x1; j < x2; ++j) {
					const Px_16s *p = psrc;
					p -= stride * rad;
					int sum = 0;
					for (int k = 0; k < d; ++k) {
						sum += *p * conv[k];
						p += stride;
					}
					*pdst = sum / divisor;
					++psrc;
					++pdst;
				}
			}
		}, y1 + t, y2, threads));
	}
	std::for_each(active_threads.begin(), active_threads.end(), [](std::thread &t) {
		t.join();
	});
	return dst;
}

Image<Px_16s> Convolutions::applyLinearConvolution(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor, int stride, int y1, int x1, int y2, int x2)
{
#ifndef NO_MT
	if (std::thread::hardware_concurrency() > 1) {
		try {
			return applyLinearConvolution_mt(src, conv, divisor, stride, y1, x1, y2, x2);
		}
        	catch (std::exception &ex) {
                	std::cerr << "MT error: " << ex.what() << std::endl;
        	}
	}
#endif
	if (conv.size() % 2 == 0) {
		throw std::runtime_error("Size of Convolution kernel must be odd.");
	}
	Image<Px_16s> dst = createSimilarImage<Px_16s>(src);
	int rad = conv.size() / 2;
	int d = 2 * rad + 1;
	for (int i = y1; i < y2; ++i) {
		const Px_16s *psrc = src.ptr(i, x1);
		Px_16s *pdst = dst.ptr(i, x1);
		for (int j = x1; j < x2; ++j) {
			const Px_16s *p = psrc;
			p -= stride * rad;
			int sum;
			for (int k = 0; k < d; ++k) {
				sum += *p * conv[k];
				p += stride;
			}
			*pdst = sum / divisor;
			++psrc;
			++pdst;
		}
	}
	return dst;
}

Image<Px_16s> Convolutions::execute_vert(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor)
{
	return applyLinearConvolution(src, conv, divisor, src.getStride(), 
								 0, -static_cast<int>(conv.size() / 2), 
								 src.getHeight(), src.getWidth() + static_cast<int>(conv.size() / 2));
}

Image<Px_16s> Convolutions::execute_hor(const Image<Px_16s> &src, const std::vector<int> &conv, int divisor)
{
	return applyLinearConvolution(src, conv, divisor, 1,
								 -static_cast<int>(conv.size() / 2), 0,
								 src.getHeight() + static_cast<int>(conv.size() / 2), src.getWidth());
}
