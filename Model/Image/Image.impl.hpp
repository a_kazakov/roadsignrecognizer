template <typename PxType1, typename PxType2>
Image<PxType1> createSimilarImage(const Image<PxType2> &src)
{
	Image<PxType1> dst;
	dst.height     = src.height;
	dst.width      = src.width;
	dst.mem_height = src.mem_height;
	dst.mem_width  = src.mem_width;
	dst.border     = src.border;
	dst.data.reset(new PxType1[dst.mem_height * dst.mem_width], std::default_delete<PxType1[]>());
	dst.first_y    = src.first_y;
	dst.first_x    = src.first_x;
	return dst;
}

template<typename PxType>
PxType *Image<PxType>::ptr(int y, int x)
{
#ifdef _DEBUG
	if (!(0 <= y + first_y && y + first_y < mem_height && 
		0 <= x + first_x && x + first_x < mem_width)) {
		throw std::out_of_range("Out of range at Image::ptr");
	}
#endif
	return data.get() + mem_width * (first_y + y) + (first_x + x);
}

template<typename PxType>
const PxType *Image<PxType>::ptr(int y, int x) const
{
#ifdef _DEBUG
	if (!(0 <= y + first_y && y + first_y < mem_height &&
		0 <= x + first_x && x + first_x < mem_width)) {
		throw std::out_of_range("Out of range at Image::ptr");
	}
#endif
	return data.get() + mem_width * (first_y + y) + (first_x + x);
}

template<typename PxType>
PxType &Image<PxType>::at(int y, int x)
{
	return *ptr(y, x);
}

template<typename PxType>
PxType Image<PxType>::at(int y, int x) const
{
	return *ptr(y, x);
}

template<typename PxType>
PxType &Image<PxType>::operator()(int y, int x)
{
	return at(y, x);
}

template<typename PxType>
PxType Image<PxType>::operator()(int y, int x) const
{
	return at(y, x);
}

template<typename PxType>
PxType *Image<PxType>::optr(int y, int x)
{
	return data.get() + mem_width * (border + y) + (border + x);
}

template<typename PxType>
const PxType *Image<PxType>::optr(int y, int x) const
{
	return data.get() + mem_width * (border + y) + (border + x);
}

template<typename PxType>
PxType &Image<PxType>::oat(int y, int x)
{
	return *optr(y, x);
}

template<typename PxType>
const PxType &Image<PxType>::oat(int y, int x) const
{
	return *optr(y, x);
}

template<typename PxType>
int Image<PxType>::getHeight() const
{
	return height;
}

template<typename PxType>
int Image<PxType>::getWidth() const
{
	return width;
}

template<typename PxType>
int Image<PxType>::getOriginalHeight() const
{
	return mem_height - 2 * border;
}

template<typename PxType>
int Image<PxType>::getOriginalWidth() const
{
	return mem_width - 2 * border;
}

template<typename PxType>
int Image<PxType>::getBorderWidth() const
{
	return border;
}

template<typename PxType>
int Image<PxType>::getStride() const
{
	return mem_width;
}

template<typename PxType>
int Image<PxType>::getNumPixels() const
{
	return mem_width * mem_height;
}

template<typename PxType>
PxType *Image<PxType>::getFirstPtr()
{
	return data.get();
}

template<typename PxType>
const PxType *Image<PxType>::getFirstPtr() const
{
	return data.get();
}

template<typename PxType>
Image<PxType>::Image(int height, int width, int border)
{
	this->height     = height;
	this->width      = width;
	this->mem_height = height + 2 * border;
	this->mem_width  = width + 2 * border;
	this->border     = border;
	this->data.reset(new PxType[this->mem_height * this->mem_width], std::default_delete<PxType[]>());
	this->first_y    = border;
	this->first_x    = border;
}

template<typename PxType>
Image<PxType>::Image(const Image<PxType> &img, bool blank_image)
{
	assign(img, false);
}

template<typename PxType>
Image<PxType> &Image<PxType>::operator = (const Image<PxType> &img)
{
	assign(img, false);
	return *this;
}

template<typename PxType>
Image<PxType>::~Image()
{
}

template<typename PxType>
void Image<PxType>::checkBorders(int size) const
{
	if (border < size) {
		// enhace borders
		int diff = size - border;
		int old_stride = getStride();
		int old_height = mem_height;
		mem_height += 2 * diff;
		mem_width  += 2 * diff;
		PxType *new_data = new PxType[mem_height * mem_width];
		PxType *pold = const_cast<PxType *>(getFirstPtr());
		PxType *pnew = new_data + diff * mem_width + diff;
		for (int i = 0; i < old_height; ++i) {
			std::memcpy(pnew, pold, old_stride * sizeof(PxType));
			pold += old_stride;
			pnew += mem_width;
		}
		data.reset(new_data, std::default_delete<PxType[]>());
		first_y += diff;
		first_x += diff;
		border = size;
	}
}

template<typename PxType>
void Image<PxType>::fillBorders() const
{
	// top + bottom
	int ow = getOriginalWidth();
	int oh = getOriginalHeight();
	for (int i = 0; i < border; ++i) {
		std::memcpy(const_cast<PxType *>(optr(-1 - i, 0)), optr(i, 0), ow * sizeof(PxType));
		std::memcpy(const_cast<PxType *>(optr(oh + i, 0)), optr(oh - i - 1, 0), ow * sizeof(PxType));
	}
	// left + right
	for (int i = -border; i < oh + border; ++i) {
		for (int j = 0; j < border; ++j) {
			const_cast<PxType &>(oat(i, -1 - j)) = oat(i, j);
			const_cast<PxType &>(oat(i, ow + j)) = oat(i, ow - 1 - j);
		}
	}
}

template<typename PxType>
void Image<PxType>::clearBorders() const
{
	// top + bottom
	for (int i = -border; i < 0; ++i) {
		std::memset(optr(i, -border), 0, mem_width * sizeof(PxType));
	}
	for (int i = height; i < height + border; ++i) {
		std::memset(optr(i, -border), 0, mem_width * sizeof(PxType));
	}
	// left + right
	for (int i = 0; i < height; ++i) {
		std::memset(optr(i, -border), 0, sizeof(PxType) * border);
		std::memset(optr(i, width), 0, sizeof(PxType) * border);
	}
}

template<typename PxType>
void Image<PxType>::assign(const Image &img, bool copy)
{
	this->height     = img.height;
	this->width      = img.width;
	this->mem_height = img.mem_height;
	this->mem_width  = img.mem_width;
	this->border     = img.border;
	this->first_y    = img.first_y;
	this->first_x    = img.first_x;
	if (copy) {
		this->data.reset(new PxType[this->mem_height * this->mem_width], std::default_delete<PxType[]>());
		std::memcpy(this->data.get(), img.data.get(), this->mem_height * this->mem_width * sizeof(PxType));
	} else {
		this->data = img.data;
	}
}

template<typename PxType>
Image<PxType> Image<PxType>::createFromROI(int border)
{
	Image<PxType> res(height, width, border < 0 ? this->border : border);
	for (int i = 0; i < height; ++i) {
		std::memcpy(res.ptr(i, 0), ptr(i, 0), width * sizeof(PxType));
	}
	return res;
}

template<typename PxType>
void Image<PxType>::saveDbg(const std::string &filename) const
{
	Image tmp = *this;
	tmp.setRoi(-border, -border, mem_height, mem_width);
	tmp.save(filename);
}

template<typename PxType>
void Image<PxType>::setRoi(int y1, int x1, int h, int w)
{
	first_y = border + y1;
	first_x = border + x1;
	height = h;
	width = w;
}

template<typename PxType>
void Image<PxType>::resetRoi()
{
	first_y = border;
	first_x = border;
	height = mem_height - 2 * border;
	width = mem_width - 2 * border;
}