#pragma once

#include <cstdint>
#include <cstring>
#include <string>
#include <stdexcept>
#include <memory>

#include "../../EasyBMP/EasyBMP.h"
// need this style for abitily to attach this file to other projects

#include "types.h"

template <typename PxType>
class Image
{
public:
	inline PxType *ptr(int y, int x);
	inline const PxType *ptr(int y, int x) const;
	inline PxType &at(int y, int x);
	inline PxType at(int y, int x) const;
	inline PxType &operator() (int y, int x);
	inline PxType operator() (int y, int x) const;

	inline PxType *optr(int y, int x);
	inline const PxType *optr(int y, int x) const;
	inline PxType &oat(int y, int x);
	inline const PxType &oat(int y, int x) const;

	inline PxType *getFirstPtr();
	inline const PxType *getFirstPtr() const;

	inline int getHeight() const;
	inline int getWidth() const;
	inline int getOriginalHeight() const;
	inline int getOriginalWidth() const;
	inline int getBorderWidth() const;
	inline int getStride() const;
	inline int getNumPixels() const;
	inline int getCellSize() const;

	void checkBorders(int size) const;
	void fillBorders() const;
	void clearBorders() const;

	Image(const std::string &filename, int border = 0);
	Image(int height = 1, int width = 1, int border = 0);
	Image(const Image &img, bool blank_image = false);
	Image & operator = (const Image &img);
	~Image();

	Image exportParams();

	void assign(const Image &img, bool copy = true);
	Image createFromROI(int border = -1);

	void save(const std::string &filename) const;
	void saveDbg(const std::string &filename) const;

	void setRoi(int y1, int x1, int h, int w);
	void resetRoi();
protected:
	mutable int first_y, first_x;
	int height, width;
	mutable int mem_height, mem_width;
	mutable int border;
	mutable std::shared_ptr<PxType> data;
private:
	template <typename PxType1, typename PxType2>
	friend Image<PxType1> createSimilarImage(const Image<PxType2> &src);
};

#include "Image.impl.hpp"
