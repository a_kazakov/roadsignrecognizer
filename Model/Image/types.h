#pragma once

#include <cstdint>

template <typename base_type, int num_fields>
struct ImageComplexType {
	base_type value[num_fields];
	inline base_type &operator[] (int offset) {
		return *(reinterpret_cast<base_type *>(value) + offset);
	}
	inline base_type  operator[] (int offset) const {
		return *(reinterpret_cast<const base_type *>(value) + offset);
	}
}
#ifndef _MSC_VER // compiler detection
__attribute__((packed))
#endif
;

typedef uint8_t                      Px_8u;
typedef int8_t                       Px_8s;

typedef uint16_t                     Px_16u;
typedef int16_t                      Px_16s;

typedef uint32_t                     Px_32u;
typedef int32_t                      Px_32s;

typedef ImageComplexType<uint8_t, 1> Px_8u1c;
typedef ImageComplexType<uint8_t, 2> Px_8u2c;
typedef ImageComplexType<uint8_t, 3> Px_8u3c;
typedef ImageComplexType<uint8_t, 4> Px_8u4c;

typedef ImageComplexType<uint16_t, 1> Px_16u1c;
typedef ImageComplexType<uint16_t, 2> Px_16u2c;
typedef ImageComplexType<uint16_t, 3> Px_16u3c;
typedef ImageComplexType<uint16_t, 4> Px_16u4c;

typedef ImageComplexType<int16_t, 1> Px_16s1c;
typedef ImageComplexType<int16_t, 2> Px_16s2c;
typedef ImageComplexType<int16_t, 3> Px_16s3c;
typedef ImageComplexType<int16_t, 4> Px_16s4c;
