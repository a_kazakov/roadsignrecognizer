#include "Image.h"

template <>
Image<Px_8u3c>::Image(const std::string &filename, int border)
{
	BMP input;
	// check input
    if (!input.ReadFromFile(filename.c_str())) {
		throw std::runtime_error(std::string("Cannot open ") + filename);
	}
	// init structure
	this->height     = input.TellHeight();
	this->width      = input.TellWidth();
	this->mem_height = height + 2 * border;
	this->mem_width  = width + 2 * border;
	this->border     = border;
	this->data.reset(new Px_8u3c[this->mem_height * this->mem_width], std::default_delete<Px_8u3c[]>());
	this->first_y    = this->first_x = border;
	// load from file
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			Px_8u3c &px = at(i, j);
			px[0] = input(j, i)->Blue;
			px[1] = input(j, i)->Green;
			px[2] = input(j, i)->Red;
		}
	}
}

template<>
void Image<Px_8u3c>::save(const std::string &filename) const
{
	BMP output;
	output.SetSize(width, height);
	output.SetBitDepth(24);
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			RGBApixel px;
			const Px_8u3c &mypx = at(i, j);
			px.Blue  = mypx[0];
			px.Green = mypx[1];
			px.Red   = mypx[2];
			output.SetPixel(j, i, px);
		}
	}
	output.WriteToFile(filename.c_str());
}

template<>
void Image<Px_8u>::save(const std::string &filename) const
{
	BMP output;
	output.SetSize(width, height);
	output.SetBitDepth(24);
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			RGBApixel px;
			Px_8u mypx = at(i, j);
			px.Blue  = mypx;
			px.Green = mypx;
			px.Red   = mypx;
			output.SetPixel(j, i, px);
		}
	}
	output.WriteToFile(filename.c_str());
}
